SELECT * FROM PETRESCUE;

-- ## Exercise 2: Aggregate Functions ## 

--Query A1: Enter a function that calculates the total cost of all animal rescues in the PETRESCUE table.
SELECT SUM(cost) from PETRESCUE;

--Query A2: Enter a function that displays the total cost of all animal rescues in the PETRESCUE table in a column called SUM_OF_COST.
SELECT SUM(cost) as SUM_OF_COST from PETRESCUE;

--Query A3: Enter a function that displays the maximum quantity of animals rescued.
SELECT MAX(quantity) from PETRESCUE;

--Query A4: Enter a function that displays the average cost of animals rescued.
SELECT AVG(cost) from PETRESCUE;

--Query A5: Enter a function that displays the average cost of rescuing a dog.
SELECT AVG(cost / quantity) from PETRESCUE
WHERE animal = 'Dog';

-- ## Exercise 3: Scalar and String Functions ##

--Query B1: Enter a function that displays the rounded cost of each rescue.
SELECT ROUND(cost) from PETRESCUE;

--Query B2: Enter a function that displays the length of each animal name.
SELECT LENGTH(animal) from PETRESCUE;

--Query B3: Enter a function that displays the animal name in each rescue in uppercase.
SELECT UCASE(animal) from PETRESCUE;

--Query B4: Enter a function that displays the animal name in each rescue in uppercase without duplications.
SELECT DISTINCT(UCASE(animal)) from PETRESCUE;

--Query B5: Enter a query that displays all the columns from the PETRESCUE table, where the animal(s) rescued are cats. Use cat in lower case in the query.
SELECT * FROM PETRESCUE
WHERE LCASE(animal) = 'cat';

-- ## Exercise 4: Date and Time Functions ## 

--Query C1: Enter a function that displays the day of the month when cats have been rescued.
SELECT DAY(rescuedate) FROM PETRESCUE where animal = 'Cat';

--Query C2: Enter a function that displays the number of rescues on the 5th day of the month.
SELECT quantity FROM PETRESCUE
WHERE DAY(rescuedate) = '05';

--Query C3: Animals rescued should see the vet within three days of arrivals. Enter a function that displays the third day from each rescue.
SELECT (rescuedate + 3 DAYS) FROM PETRESCUE;

--Query C4: Enter a function that displays the length of time the animals have been rescued; the difference between today’s date and the rescue date.
SELECT (CURRENT_DATE - rescuedate) FROM PETRESCUE;









